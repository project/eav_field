<?php

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\eav_field\Entity\EavAttribute;

/**
 * Add "status" base field to eav_attribute entity type.
 */
function eav_field_update_9201(): void {
  $field_definition = BaseFieldDefinition::create('boolean')
    ->setLabel(t('Active'))
    ->setDefaultValue(TRUE)
    ->setInitialValue(TRUE)
    ->setDisplayOptions('form', [
      'type' => 'boolean_checkbox',
      'weight' => 100,
    ])
    ->setDisplayConfigurable('form', TRUE);

  \Drupal::entityDefinitionUpdateManager()->installFieldStorageDefinition('status', 'eav_attribute', 'eav_field', $field_definition);
}

/**
 * Add "created" base field to eav_attribute entity type.
 */
function eav_field_update_9202(): void {
  $field_definition = BaseFieldDefinition::create('created')
    ->setLabel(t('Created'))
    ->setDescription(t('The time that the attribute was created.'))
    ->setDefaultValueCallback(EavAttribute::class . '::getRequestTime');

  \Drupal::entityDefinitionUpdateManager()->installFieldStorageDefinition('created', 'eav_attribute', 'eav_field', $field_definition);
}
