Installation & usage:
---

1. Download module: composer require drupal/eav_field

2. Enable module: vendor/bin/drush en eav_field

3. Go to "admin/structure/eav/attributes/fields/eav_attribute.eav_attribute.category", check vocabulary and click "Save settings".

4. Go to "admin/structure/eav/attributes" page and add attributes.

5. Go to entity "Manage fields" page and add field "EAV attributes".

6. Go to entity "Manage form display" page and hide EAV attributes field, because field widget will be available on new local task.

7. Create entity with category (optional).

8. Go to entity "Edit EAV attributes" tab, set attributes values and click "Save".

9. If you have a lot entities with EAV field (10000+) and many EAV attributes (100+) you need to manualy add an database index by value column in eav_value__* tables.
