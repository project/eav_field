<?php

namespace Drupal\eav_field\Routing;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Symfony\Component\Routing\Route;
use Drupal\eav_field\Form\EavWidgetForm;

class EavFieldRoutes {

  /**
   * Dynamic module routes.
   */
  public function routes(): array {
    $routes = [];

    $entity_type_manager = \Drupal::entityTypeManager();
    $entity_field_manger = \Drupal::service('entity_field.manager'); /** @var EntityFieldManagerInterface $entity_field_manger */
    $eav_fields_map = $entity_field_manger->getFieldMapByFieldType('eav');

    foreach ($eav_fields_map as $entity_type_id => $eav_fields) {
      $entity_type = $entity_type_manager->getDefinition($entity_type_id);

      $route = [
        'path' => $entity_type->getLinkTemplate('canonical') . '/edit-eav/{field_name}',
        'defaults' => [
          '_form' => EavWidgetForm::class,
        ],
        'requirements' => [
          '_entity_access' => $entity_type_id . '.update',
          '_custom_access' => 'eav_field_edit_eav_route_access_check', // For local task visibility
        ],
        'options' => [
          '_admin_route' => TRUE,
          // @TODO Use $route->getOptions() (?)
          'parameters' => [
            $entity_type_id => [
              'type' => 'entity:' . $entity_type_id,
            ],
          ],
        ],
      ];

      $routes["entity.$entity_type_id.edit_eav"] = new Route($route['path'], $route['defaults'], $route['requirements'], $route['options']);
    }

    return $routes;
  }

}
