<?php

namespace Drupal\eav_field;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\eav_field\Entity\EavValue;

class EavValueStorage extends SqlContentEntityStorage {

  /**
   * {@inheritDoc}
   */
  public function loadMultiple(array $ids = NULL): array {
    // Set unlimited cardinality to value fields in order to load all field values from database.
    // See \Drupal\Core\Entity\Sql\SqlContentEntityStorage::loadFromDedicatedTables()
    // See \Drupal\Tests\eav_field\Functional\EavFieldTest::_testAttributesValuesFormSubmit_MultiStringAndString()
    $field_definitions = $this->entityFieldManager->getFieldDefinitions('eav_value', 'eav_value'); /** @var BaseFieldDefinition[] $field_definitions */
    foreach (EavValue::getFieldTypes() as $field_type => $field_settings) {
      $field_definitions[$field_type . '_value']->getFieldStorageDefinition()->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    }

    return parent::loadMultiple($ids);
  }

}
