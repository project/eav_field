<?php

namespace Drupal\eav_field\EventSubscriber;

use Drupal\search_api_solr\Event\PostCreateIndexDocumentEvent;
use Drupal\search_api_solr\Event\SearchApiSolrEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EavFieldEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    if (class_exists(SearchApiSolrEvents::class)) {
      return [
        SearchApiSolrEvents::POST_CREATE_INDEX_DOCUMENT => 'onSearchApiSolrPostCreateIndexDocument',
      ];
    }

    return [];
  }

  /**
   * Search API solr post create index document event.
   */
  public function onSearchApiSolrPostCreateIndexDocument(PostCreateIndexDocumentEvent $event) {
    $solr_document = $event->getSolariumDocument(); /** @var \Solarium\QueryType\Update\Query\Document $solr_document */

    // Remove sort fields added in SearchApiSolrBackend
    /** @see \Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend::getDocuments:1328 */
    foreach ($solr_document->getFields() as $field_name => $field_value) {
      if (preg_match('/^sort_.*_eav_field_/', $field_name)) {
        $solr_document->removeField($field_name);
      }
    }
  }

}
