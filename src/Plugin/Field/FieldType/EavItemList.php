<?php

namespace Drupal\eav_field\Plugin\Field\FieldType;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eav_field\Entity\EavAttributeInterface;
use Drupal\eav_field\Entity\EavValue;
use Drupal\eav_field\Entity\EavValueInterface;

class EavItemList extends EntityReferenceFieldItemList implements EavItemListInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultValuesForm(array &$form, FormStateInterface $form_state): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update): void {
    // Delete orhpaned eav_value entities
    if ($update) {
      $entity = $this->getEntity();
      $original_entity = $entity->original; /** @var EntityInterface $original_entity */
      $new_eav_value_ids = $this->getValueEntitiesIds();
      $eav_field_name = $this->getFieldDefinition()->getName();
      $original_eav_field_items = $original_entity->get($eav_field_name); /** @var EavItemListInterface $original_eav_field_items */

      foreach ($original_eav_field_items->referencedEntities() as $original_eav_value_entity) {
        if (!in_array($original_eav_value_entity->id(), $new_eav_value_ids)) {
          $original_eav_value_entity->delete();
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function delete(): void {
    /** @var EavValueInterface $eav_value_entity */
    foreach ($this->referencedEntities() as $eav_value_entity) {
      $eav_value_entity->delete();
    }

    parent::delete();
  }

  /**
   * {@inheritdoc}
   */
  public function getItemByAttribute(int|string|EavAttributeInterface $attribute): ?EavItemInterface {
    $attribute_id = NULL;
    $attribute_machine_name = NULL;

    if (is_numeric($attribute)) {
      $attribute_id = $attribute;
    }
    elseif (is_string($attribute)) {
      $attribute_machine_name = $attribute;
    }
    elseif ($attribute instanceof EavAttributeInterface) {
      $attribute_id = $attribute->id();
    }

    /** @var EavItemInterface $eav_item */
    foreach ($this as $eav_item) {
      $eav_value = $eav_item->getValueEntity();

      if (!$eav_value) {
        continue;
      }

      // By id
      if ($attribute_id && $attribute_id == $eav_value->getAttributeId()) {
        return $eav_item;
      }
      // By machine name
      if ($attribute_machine_name && $attribute_machine_name == $eav_value->getAttributeEntity()?->getMachineName()) {
        return $eav_item;
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function referencedEntities(): array {
    return parent::referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function referencedEntitiesIds(): array {
    return array_filter(array_map(function (EntityInterface $entity) {
      return $entity->id();
    }, $this->referencedEntities()));
  }

  /**
   * {@inheritdoc}
   */
  public function getValueEntitiesIds(): array {
    return $this->referencedEntitiesIds();
  }

  /**
   * {@inheritdoc}
   */
  public function generateSampleItems($count = NULL): void {
    $attribute_storage = \Drupal::entityTypeManager()->getStorage('eav_attribute');
    $attributes = $attribute_storage->loadByHostEntityCategory($this->getEntity());

    if ($count) {
      $attributes = array_slice($attributes, 0, $count);
    }

    $values = [];

    foreach ($attributes as $attribute) {
      $eav_value = EavValue::create(['aid' => $attribute->id()]);
      $attribute->configureValueFieldDefinition();
      $eav_value->getValueFieldItems()->generateSampleItems();

      $values[] = $eav_value;
    }

    $this->setValue($values);
  }

}
