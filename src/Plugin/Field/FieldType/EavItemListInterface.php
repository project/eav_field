<?php

namespace Drupal\eav_field\Plugin\Field\FieldType;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\eav_field\Entity\EavAttributeInterface;
use Drupal\eav_field\Entity\EavValueInterface;

interface EavItemListInterface extends EntityReferenceFieldItemListInterface {

  /**
   * Return field item by attribute.
   *
   * @param int|string|EavAttributeInterface $attribute Attribute id, machine name or EavAttribute entity
   */
  public function getItemByAttribute(int|string|EavAttributeInterface $attribute): ?EavItemInterface;

  /**
   * Return eav_value entities.
   *
   * @return EavValueInterface[]
   */
  public function referencedEntities(): array;

  /**
   * Return eav_value entities ids.
   */
  public function referencedEntitiesIds(): array;

  /**
   * Return eav_value entities ids.
   */
  public function getValueEntitiesIds(): array;

}
