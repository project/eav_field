<?php

namespace Drupal\eav_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;
use Drupal\eav_field\Entity\EavValueInterface;
use Drupal\Core\Field\Attribute\FieldType;

#[FieldType(
  id: 'eav',
  label: new TranslatableMarkup('EAV attributes'),
  description: new TranslatableMarkup('A field containing many entity attributes and their values'),
  category: 'general',
  default_widget: 'eav_widget',
  default_formatter: 'eav_formatter',
  list_class: EavItemList::class,
  cardinality: FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
)]
class EavItem extends EntityReferenceItem implements EavItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings(): array {
    return [
      'target_type' => 'eav_value',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['aid'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('EAV Attribute ID'))
      ->setSetting('unsigned', TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    $schema = parent::schema($field_definition);

    $schema['columns']['aid'] = [
      'description' => 'EAV Attribute id.',
      'type' => 'int',
      'unsigned' => TRUE,
    ];

    $schema['indexes']['aid'] = ['aid'];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(): void {
    if ($eav_value = $this->getValueEntity()) {
      if ($eav_value->isNew()) {
        $eav_value->save();
      }

      $this->target_id = $eav_value->id();
      $this->aid = $eav_value->getAttributeId();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getPreconfiguredOptions(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getValueEntity(): ?EavValueInterface {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function viewValueField() {
    return $this->getValueEntity()->viewValueField();
  }

}
