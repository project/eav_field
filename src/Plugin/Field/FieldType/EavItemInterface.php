<?php

namespace Drupal\eav_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\eav_field\Entity\EavValueInterface;

interface EavItemInterface extends FieldItemInterface {

  /**
   * Return eav_value entity.
   */
  public function getValueEntity(): ?EavValueInterface;

  /**
   * Format value field in selected formatter.
   *
   * @return array|string
   */
  public function viewValueField();

}
