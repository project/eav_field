<?php

namespace Drupal\eav_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\eav_field\Plugin\Field\FieldType\EavItemListInterface;

#[FieldFormatter(
  id: 'eav_formatter',
  label: new TranslatableMarkup('Boolean'),
  field_types: ['eav'],
)]
class EavFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'only_primary' => FALSE,
      'show_empty' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form['only_primary'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show only primary attributes'),
      '#default_value' => $this->getSetting('only_primary'),
    ];

    $form['show_empty'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show empty field'),
      '#default_value' => $this->getSetting('show_empty'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    /** @var EavItemListInterface $items */

    $elements = [
      '#is_multiple' => FALSE,
    ];

    $formatter_settings = $this->getSettings();

    if ($items->count() > 0 || $formatter_settings['show_empty']) {
      $elements[0] = [
        '#theme' => 'eav_list',
        '#values' => $items->referencedEntities(),
        '#host_entity' => $items->getEntity(),
        '#field_name' => $items->getName(),
        '#only_primary' => $formatter_settings['only_primary'],
        '#context' => [
          'entity' => $items->getEntity(),
          'view_mode' => $this->viewMode,
          'field_name' => $items->getName(),
        ],
      ];
    }

    return $elements;
  }

}
