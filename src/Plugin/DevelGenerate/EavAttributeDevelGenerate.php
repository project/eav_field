<?php

namespace Drupal\eav_field\Plugin\DevelGenerate;

use Drupal\Component\Utility\Random;
use Drupal\Core\Form\FormStateInterface;
use Drupal\devel_generate\DevelGenerateBase;
use Drupal\eav_field\Entity\EavAttribute;

/**
 * @DevelGenerate(
 *   id = "eav_attribute",
 *   label = @Translation("EAV attributes"),
 *   description = @Translation("Generate a given number of EAV attributes."),
 *   url = "eav_attribute",
 *   permission = "administer devel_generate",
 *   settings = {
 *     "num" = 10,
 *     "kill" = FALSE,
 *     "category" = NULL,
 *     "terms" = NULL,
 *   },
 * )
 */
class EavAttributeDevelGenerate extends DevelGenerateBase {

  /**
   * {@inheritDoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form['num'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of attributes'),
      '#default_value' => $this->getSetting('num'),
      '#required' => TRUE,
      '#min' => 0,
    ];

    $form['category'] = [
      '#type' => 'select',
      '#title' => $this->t('Category'),
      '#options' => [
        'none' => $this->t('None'),
        'random' => $this->t('Random'),
        'selected' => $this->t('Selected terms'),
        'random_selected' => $this->t('Random from selected terms'),
      ],
    ];

    $form['terms'] = [
      '#type' => 'select',
      '#title' => $this->t('Terms'),
      '#options' => $this->getTermsOptions(),
      '#default_value' => $this->getSetting('category'),
      '#multiple' => TRUE,
      '#size' => 10,
      '#states' => [
        'visible' => [
          ':input[name="category"]' => [
            ['value' => 'selected'],
            ['value' => 'random_selected'],
          ],
        ],
      ],
    ];

    $form['kill'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete all existing attributes'),
      '#default_value' => $this->getSetting('kill'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  protected function generateElements(array $values): void {
    if ($values['kill']) {
      $attribute_storage = \Drupal::entityTypeManager()->getStorage('eav_attribute');
      $attributes = $attribute_storage->loadMultiple();
      $attribute_storage->delete($attributes);

      $this->setMessage($this->formatPlural(count($attributes), '@count item deleted', '@count items deleted.'));
    }

    $categories_ids = [];
    if ($values['category'] == 'random') {
      $categories_ids = array_keys($this->getTermsOptions());
    }

    $random_utility = new Random();

    for ($i = 1; $i <= $values['num']; $i++) {
      $attribute_label = ucfirst($random_utility->word(random_int(5, 10)));

      $attribute_category = NULL;
      if ($values['category'] == 'random') {
        $attribute_category = $categories_ids[array_rand($categories_ids)];
      }
      elseif ($values['category'] == 'selected') {
        $attribute_category = $values['terms'];
      }
      elseif ($values['category'] == 'random_selected') {
        $attribute_category = $values['terms'][array_rand($values['terms'])];
      }

      $attribute = EavAttribute::create([
        'label' => $attribute_label,
        'machine_name' => strtolower($attribute_label),
        'category' => $attribute_category,
      ]);

      $attribute->save();
    }
    $this->setMessage($this->formatPlural($values['num'], 'Created @count item', 'Created @count items'));
  }

  /**
   * {@inheritDoc}
   */
  public function validateDrushParams(array $args, array $options = []) {

  }

  /**
   * Return terms options.
   */
  protected function getTermsOptions(): array {
    $attribute_storage = \Drupal::entityTypeManager()->getStorage('eav_attribute');
    $attribute_category_field = $attribute_storage->getCategoryFieldDefinition();
    $dummy_attribute = EavAttribute::create([]);
    return $attribute_category_field
      ->getFieldStorageDefinition()
      ->getOptionsProvider('target_id', $dummy_attribute)
      ->getSettableOptions(\Drupal::currentUser());
  }

}
