<?php

namespace Drupal\eav_field\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;

class EavFieldLocalTaskDeriver extends DeriverBase {

  /**
   * {@inheritDoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $entity_field_manger = \Drupal::service('entity_field.manager'); /** @var EntityFieldManagerInterface $entity_field_manger */
    $eav_fields_map = $entity_field_manger->getFieldMapByFieldType('eav');

    foreach ($eav_fields_map as $entity_type_id => $eav_fields) {
      foreach ($eav_fields as $eav_field_name => $eav_field_info) {
        $entity_bundle_name = current($eav_field_info['bundles']);
        $eav_field_label = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type_id, $entity_bundle_name)[$eav_field_name]->getLabel();

        $this->derivatives[$entity_type_id . '_' . $eav_field_name] = [
          'title' => t('Edit @label', ['@label' => mb_strtolower($eav_field_label)]),
          'route_name' => "entity.$entity_type_id.edit_eav",
          'route_parameters' => ['field_name' => $eav_field_name],
          'base_route' => "entity.$entity_type_id.canonical",
          'weight' => 20,
        ] + $base_plugin_definition;
      }
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
