<?php

namespace Drupal\eav_field\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\eav_field\Entity\EavValue;

/**
 * @QueueWorker(
 *   id = "delete_eav_value_entities",
 *   title = @Translation("Delete eav_value entities"),
 *   cron = {"time" = 10}
 * )
 */
class DeleteEavValueEntities extends QueueWorkerBase {

  /**
   * {@inheritDoc}
   */
  public function processItem($data): void {
    if ($eav_value = EavValue::load($data['vid'])) {
      $eav_value->delete();
    }
  }

}
