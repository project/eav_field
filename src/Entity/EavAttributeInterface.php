<?php

namespace Drupal\eav_field\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\taxonomy\TermInterface;

interface EavAttributeInterface extends ContentEntityInterface {

  /**
   * Load attributes by properties.
   *
   * @return EavAttributeInterface[]
   */
  public static function loadByProperties(array $values): array;

  /**
   * Load attribute by machine name.
   */
  public static function loadByMachineName(string $machine_name): ?EavAttributeInterface;

  /**
   * Load attribute by label.
   */
  public static function loadByLabel(string $label): ?EavAttributeInterface;

  /**
   * Load unchanged attribute.
   */
  public function loadUnchanged(): ?EavAttributeInterface;

  /**
   * Return administrative label.
   */
  public function getAdministrativeLabel(): string;

  /**
   * Return attribute machine name.
   */
  public function getMachineName(): ?string;

  /**
   * Set attribute machine name.
   */
  public function setMachineName(string $machine_name): self;

  /**
   * Return attribute weight.
   */
  public function getWeight(): ?int;

  /**
   * Set attribute weight.
   */
  public function setWeight(int $weight): EavAttributeInterface;

  /**
   * Return "value field" type.
   */
  public function getValueType(): string;

  /**
   * Set "value field" type.
   */
  public function setValueType(string $value_type): EavAttributeInterface;

  /**
   * Return "value field" name.
   */
  public function getValueFieldName(): string;

  /**
   * Return "value field" storage config array.
   */
  public function getValueStorageConfigArray(bool $with_defaults = TRUE): array;

  /**
   * Return "value field" cardinality.
   */
  public function getValueCardinality(): int;

  /**
   * Return TRUE if "value field" is multiple.
   */
  public function valueIsMultiple(): bool;

  /**
   * Return "value field" config array.
   */
  public function getValueConfigArray(bool $with_defaults = TRUE): array;

  /**
   * Return value field default values.
   */
  public function getValueFieldDefaultValues(): ?array;

  /**
   * Return "value field" widget type.
   */
  public function getValueWidgetType(): string;

  /**
   * Return "value field" widget settings.
   */
  public function getValueWidgetSettings(): array;

  /**
   * Return "value field" formatter type.
   */
  public function getValueFormatterType(): ?string;

  /**
   * Return "value field" formatter settings.
   */
  public function getValueFormatterSettings(): array;

  /**
   * Return TRUE if attribute is active.
   */
  public function isActive(): bool;

  /**
   * Return TRUE if attribute is primary.
   */
  public function isPrimary(): bool;

  /**
   * Return value field definition.
   */
  public function getValueFieldDefinition(): FieldDefinitionInterface;

  /**
   * Configure value field definition and return him definition.
   */
  public function configureValueFieldDefinition(): BaseFieldDefinition;

  /**
   * Return categories ids.
   *
   * @return integer[]
   */
  public function getCategoriesIds(): array;

  /**
   * Set category field.
   *
   * @param TermInterface[]|int[] $categories Categories entities or ids
   */
  public function setCategories(array $categories): EavAttributeInterface;

  /**
   * Return description
   */
  public function getDescription(): ?array;

  /**
   * Return EAV attributes max weight.
   */
  public static function getMaxWeight(): int;

  /**
   * Return "value widget type" field allowed values.
   */
  public static function getValueWidgetTypeAllowedValues(FieldStorageDefinitionInterface $field_storage_definition, EavAttributeInterface $attribute = NULL, bool &$cacheable = TRUE): array;

  /**
   * Return "value formatter type" field allowed values.
   */
  public static function getValueFormatterTypeAllowedValues(FieldStorageDefinitionInterface $field_storage_definition, EavAttributeInterface $attribute = NULL, bool &$cacheable = TRUE): array;

  /**
   * Helper callback for uasort().
   */
  public static function sort(EavAttributeInterface $a, EavAttributeInterface $b): int;

}
