<?php


namespace Drupal\eav_field\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;

interface EavValueInterface extends ContentEntityInterface {

  /**
   * {@inheritDoc}
   */
  public static function load($id): ?EavValueInterface;

  /**
   * {@inheritDoc}
   */
  public static function create(array $values = []): EavValueInterface;

  /**
   * Return supported value field types.
   */
  public static function getFieldTypes(): array;

  /**
   * Return attribute id.
   */
  public function getAttributeId(): ?int;

  /**
   * Return attribute entity.
   */
  public function getAttributeEntity(): ?EavAttributeInterface;

  /**
   * Return value field name.
   */
  public function getValueFieldName(): ?string;

  /**
   * Return value field items.
   */
  public function getValueFieldItems(): ?FieldItemListInterface;

  /**
   * Return value field values.
   */
  public function getValueFieldValues(): ?array;

  /**
   * Return value field main values.
   */
  public function getValueFieldMainValues();

  /**
   * Set value field value.
   */
  public function setValueFieldValue($value): void;

  /**
   * Format value field in selected formatter.
   *
   * @return array|string
   */
  public function viewValueField();

}
