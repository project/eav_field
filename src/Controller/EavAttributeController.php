<?php

namespace Drupal\eav_field\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\FormState;
use Drupal\eav_field\Entity\EavAttributeInterface;
use Drupal\eav_field\Form\EavAttributeValueFieldSettingsForm;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\eav_field\Form\EavAttributeValueStorageSettingsForm;

class EavAttributeController extends ControllerBase {

  /**
   * Show attribute value storage settings form.
   */
  public function valueStorageSettingsForm(EavAttributeInterface $eav_attribute): array {
    $override_result = $this->overrideValueFieldDefinitions($eav_attribute);
    $field_storage_config = $override_result['field_storage_config']; /** @var FieldStorageConfig $field_storage_config */
    $field_config = $override_result['field_config']; /** @var FieldConfig $field_config */

    // @see \Drupal\Core\Entity\EntityTypeManager::getFormObject()
    $form_object = \Drupal::classResolver(EavAttributeValueStorageSettingsForm::class); /** @var EavAttributeValueStorageSettingsForm $form_object */
    $form_object
      ->setEntity($field_storage_config)
      ->setOperation('edit')
      ->setModuleHandler($this->moduleHandler());

    $form_state = new FormState();
    // See how to fill $ids in \Drupal\field_ui\Form\FieldStorageConfigEditForm::form()
    $form_state->setFormState([
      'field_config' => $field_config,
      'entity_type_id' => 'eav_value',
      'bundle' => 'eav_value',
      'attribute' => $eav_attribute,
    ]);

    return $this->formBuilder()->buildForm($form_object, $form_state);
  }

  /**
   * Show attribute value field settings form.
   */
  public function valueFieldSettingsForm(EavAttributeInterface $eav_attribute): array {
    $override_result = $this->overrideValueFieldDefinitions($eav_attribute);
    $field_config = $override_result['field_config']; /** @var FieldConfig $field_config */

    /* @see \Drupal\Core\Entity\EntityTypeManager::getFormObject() */
    $form_object = \Drupal::classResolver(EavAttributeValueFieldSettingsForm::class); /** @var EavAttributeValueFieldSettingsForm $form_object */
    $form_object
      ->setEntity($field_config)
      ->setOperation('edit')
      ->setModuleHandler($this->moduleHandler())
      ->setEntityTypeManager($this->entityTypeManager())
      ->setStringTranslation($this->getStringTranslation());

    $form_state = new FormState();
    // See how to fill $ids in \Drupal\field_ui\Form\FieldConfigEditForm::form()
    $form_state->setFormState([
      'entity_type_id' => 'eav_value',
      'bundle' => 'eav_value',
      'attribute' => $eav_attribute,
    ]);

    return $this->formBuilder()->buildForm($form_object, $form_state);
  }

  /**
   * Override value field definitions.
   */
  protected function overrideValueFieldDefinitions(EavAttributeInterface $eav_attribute): array {
    /** @var FieldStorageConfig $field_storage_config */
    $field_storage_config = FieldStorageConfig::create($eav_attribute->getValueStorageConfigArray());
    /** @var FieldConfig $field_config */
    $field_config = FieldConfig::create($eav_attribute->getValueConfigArray() + ['field_storage' => $field_storage_config]);

    $field_definition = $eav_attribute->configureValueFieldDefinition();
    $field_storage_definition = $field_definition->getFieldStorageDefinition(); /** @var BaseFieldDefinition $field_storage_definition */

    return [
      'field_storage_config' => $field_storage_config,
      'field_config' => $field_config,
      'field_storage_definition' => $field_storage_definition,
      'field_definition' => $field_definition,
    ];
  }

}
