<?php

namespace Drupal\eav_field\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eav_field\Entity\EavAttributeInterface;
use Symfony\Component\HttpFoundation\Response;

class EavAttributeForm extends ContentEntityForm {

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $attribute = $this->getEntity(); /** @var EavAttributeInterface $attribute */

    if ($this->getOperation() == 'edit') {
      $form['#title'] = $this->t('Editing attribute "@attribute"', ['@attribute' => $attribute->getAdministrativeLabel()]);
    }

    $form['value_type']['widget']['#ajax'] = [
      'callback' => '::ajaxValueTypeChanged',
    ];

    if ($selected_value_type = $form_state->getValue(['value_type', 0, 'value'])) {
      $attribute->set('value_type', $selected_value_type);

      // Change "value_widget_type" and "value_formatter_type" field options depending on selected "value_type" value.
      $form['value_widget_type']['widget']['#options'] = $this->getFieldAllowedValues($attribute->get('value_widget_type'));
      $form['value_formatter_type']['widget']['#options'] = $this->getFieldAllowedValues($attribute->get('value_formatter_type'));
    }

    return $form;
  }

  /**
   * AJAX callback on value_type changed.
   */
  public function ajaxValueTypeChanged(array $form, FormStateInterface $form_state): Response {
    $response = new AjaxResponse();

    $response->addCommand(new ReplaceCommand('[data-drupal-selector="edit-value-widget-type-wrapper"]', $form['value_widget_type']));
    $response->addCommand(new ReplaceCommand('[data-drupal-selector="edit-value-formatter-type-wrapper"]', $form['value_formatter_type']));

    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $attribute = $this->getEntity(); /** @var EavAttributeInterface $attribute */
    $operation = $this->getOperation();

    if ($operation == 'edit') {
      $unchanged_attribute = $attribute->loadUnchanged();

      if ($attribute->getValueType() != $unchanged_attribute->getValueType()) {
        $attribute->set('value_storage_config', NULL);
        $attribute->set('value_config', NULL);
        $attribute->set('value_widget_settings', NULL);
        $attribute->set('value_formatter_settings', NULL);
      }
      if ($attribute->getValueWidgetType() != $unchanged_attribute->getValueWidgetType()) {
        $attribute->set('value_widget_settings', NULL);
      }
      if ($attribute->getValueFormatterType() != $unchanged_attribute->getValueFormatterType()) {
        $attribute->set('value_formatter_settings', NULL);
      }
    }

    // Save attribute and generate attribute id
    $return = $attribute->save();

    // Redirect to next step
    if ($operation == 'add') {
      $form_state->setRedirect('entity.eav_attribute.value_storage_form', ['eav_attribute' => $attribute->id()], ['query' => ['operation' => 'add']]);
    }
    else {
      $this->messenger()->addMessage($this->t('Attribute saved.'));
    }

    return $return;
  }

  /**
   * Return field allowed values.
   */
  protected function getFieldAllowedValues(FieldItemListInterface $field_items): array {
    return options_allowed_values($field_items->getFieldDefinition()->getFieldStorageDefinition(), $field_items->getEntity());
  }

}
