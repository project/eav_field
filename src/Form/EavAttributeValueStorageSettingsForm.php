<?php

namespace Drupal\eav_field\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eav_field\Entity\EavAttributeInterface;
use Drupal\field_ui\Form\FieldStorageConfigEditForm;

class EavAttributeValueStorageSettingsForm extends FieldStorageConfigEditForm {

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $attribute = $form_state->get('attribute') /** @var EavAttributeInterface $attribute */;
    $attribute_type = $attribute->getValueType();

    if (isset($form['settings'])) {
      $form['settings']['#type'] = 'fieldset';
      $form['settings']['warning'] = [
        '#theme' => 'status_messages',
        '#message_list' => [
          'warning' => [
            $this->t('Recommends keep settings are default.'),
          ],
        ],
        '#weight' => -1000,
      ];
    }

    // Undisable disabled settings if eav_value entities already created
    if ($attribute_type == 'string') {
      $form['settings']['max_length']['#disabled'] = FALSE;
    }
    elseif ($attribute_type == 'decimal') {
      $form['settings']['precision']['#disabled'] = FALSE;
      $form['settings']['scale']['#disabled'] = FALSE;
    }
    elseif ($attribute_type == 'entity_reference') {
      $form['settings']['target_type']['#disabled'] = FALSE;
    }

    $form['#title'] = $this->t('Storage settings of attribute "@attribute"', ['@attribute' => $attribute->getAdministrativeLabel()]);
    $form['#prefix'] = '';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $elements = EntityForm::actions($form, $form_state);
    $elements['submit']['#value'] = $this->entity->isNew() ? $this->t('Continue') : $this->t('Save');

    return $elements;
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $attribute = $form_state->get('attribute') /** @var EavAttributeInterface $attribute */;

    $field_storage_config = $this->entity;
    $field_storage_config_array = array_intersect_key($field_storage_config->toArray(), array_flip([
      'settings',
      'cardinality',
    ]));

    $attribute->set('value_storage_config', $field_storage_config_array);
    $return = $attribute->save();

    // Redirect to next step
    if ($this->getRequest()->query->get('operation') == 'add') {
      $form_state->setRedirect('entity.eav_attribute.value_field_form', ['eav_attribute' => $attribute->id()], ['query' => ['operation' => 'add']]);
    }
    else {
      $this->messenger()->addMessage($this->t('Storage settings saved.'));
    }

    return $return;
  }

}
