<?php

namespace Drupal\eav_field\Form;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Field\WidgetPluginManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EavWidgetForm extends FormBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  protected EntityFieldManagerInterface $entityFieldManager;

  protected WidgetPluginManager $fieldWidgetManager;

  /**
   * Form constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    WidgetPluginManager $field_widget_manager,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->fieldWidgetManager = $field_widget_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.widget')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'eav_entity_attributes_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $field_name = NULL): array {
    $entity = eav_field_get_entity_from_route_match($this->getRouteMatch());
    $eav_field_definition = $entity->get($field_name)->getFieldDefinition();
    $eav_field_name = $eav_field_definition->getName();
    $form_display = EntityFormDisplay::collectRenderDisplay($entity, 'edit');

    $eav_widget = $this->fieldWidgetManager->getInstance([
      'field_definition' => $eav_field_definition,
      'configuration' => $form_display->getComponent($eav_field_name) ?: [],
    ]);

    $form['#parents'] = $form['#parents'] ?? [];
    $form[$eav_field_name] = $eav_widget->form($entity->get($eav_field_name), $form, $form_state);

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    if (!$entity->get($eav_field_name)->isEmpty()) {
      $form['actions']['delete'] = [
        '#type' => 'submit',
        '#value' => $this->t('Clear all attributes value'),
        '#button_type' => 'danger',
        '#submit' => ['::clearAllValues'],
      ];
    }

    $form['#title'] = $this->t('Editing attributes of "@entity_label"', ['@entity_label' => $entity->label()]);

    $form_state->set('entity', $entity);
    $form_state->set('eav_widget', $eav_widget);

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $host_entity = $form_state->get('entity'); /** @var FieldableEntityInterface $host_entity */
    $eav_widget = $form_state->get('eav_widget'); /** @var WidgetInterface $eav_widget */
    $eav_field_name = $form_state->getBuildInfo()['args'][0];

    $eav_widget->extractFormValues($host_entity->get($eav_field_name), $form, $form_state);
    $host_entity->save();

    $this->messenger()->addMessage($this->t('Values saved.'));
  }

  /**
   * "delete" button submit callback.
   */
  public function clearAllValues(array &$form, FormStateInterface $form_state): void {
    $entity = $form_state->get('entity'); /** @var EntityInterface $entity */
    $eav_field_name = $form_state->getBuildInfo()['args'][0];

    // Delete all eav_value entities and clear eav field value
    $entity->set($eav_field_name, NULL);
    $entity->save();

    $this->messenger()->addMessage($this->t('Values cleared.'));
  }

}
