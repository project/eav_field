<?php

namespace Drupal\eav_field\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\eav_field\Entity\EavAttributeInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\field_ui\Form\FieldConfigEditForm;

class EavAttributeValueFieldSettingsForm extends FieldConfigEditForm {

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $attribute = $form_state->get('attribute') /** @var EavAttributeInterface $attribute */;

    $form['#title'] = $this->t('Field settings of attribute "@attribute"', ['@attribute' => $attribute->getAdministrativeLabel()]);
    $form['label']['#access'] = FALSE;

    // Remove storage settings subform.
    // Not used "unset($form['field_storage'])" because field_ui_form_field_config_edit_form_alter() using this element.
    // @TODO Merge forms
    $form['field_storage']['#access'] = FALSE;
    $form['field_storage']['subform'] = [];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Build field config entity from form values
    // Not used parent::submitForm() because parent method is trying to create field storage config.
    EntityForm::submitForm($form, $form_state);

    // Handle the default value.
    /** @see \Drupal\field_ui\Form\FieldConfigEditForm::submitForm() */
    $default_value = [];
    if (isset($form['default_value']) && (!isset($form['set_default_value']) || $form_state->getValue('set_default_value'))) {
      $items = $this->getTypedData($this->entity, $form['#entity']);
      $default_value = $items->defaultValuesFormSubmit($form['default_value'], $form, $form_state);
    }
    $this->entity->setDefaultValue($default_value);
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $attribute = $form_state->get('attribute') /** @var EavAttributeInterface $attribute */;

    $field_config = $this->entity; /** @var FieldStorageConfigInterface $field_config */
    $field_config_array = array_intersect_key($field_config->toArray(), array_flip([
      'settings',
      'default_value',
      'description',
      'required',
    ]));

    $attribute->set('value_config', $field_config_array);
    $attribute->save();

    // Redirect to next step
    if ($this->getRequest()->query->get('operation') == 'add') {
      $form_state->setRedirect('entity.eav_attribute.value_widget_form', ['eav_attribute' => $attribute->id()], ['query' => ['operation' => 'add']]);
    }
    else {
      $this->messenger()->addMessage($this->t('Field settings saved.'));
    }
  }

  /**
   * Change method visibility from private to protected.
   *
   * @see \Drupal\field_ui\Form\FieldConfigEditForm::getTypedData()
   */
  protected function getTypedData(FieldConfigInterface $field_config, FieldableEntityInterface $parent): TypedDataInterface {
    $this->typedDataManager->clearCachedDefinitions();
    $entity_adapter = EntityAdapter::createFromEntity($parent);
    return $this->typedDataManager->create($field_config, $field_config->getDefaultValue($parent), $field_config->getName(), $entity_adapter);
  }

}
