<?php

namespace Drupal\eav_field;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\eav_field\Entity\EavAttribute;
use Drupal\eav_field\Entity\EavAttributeInterface;

class EavAttributeListBuilder extends EntityListBuilder implements FormInterface {

  protected FormBuilderInterface $formBuilder;

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header = [
      'label' => $this->t('Label'),
      'aid' => $this->t('ID'),
      'machine_name' => $this->t('Machine name'),
      'value_type' => $this->t('Value type'),
      'category' => $this->t('Category'),
      'weight' => $this->t('Weight'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var EavAttributeInterface $entity */

    $categories = [];
    if ($entity->hasField('category')) {
      foreach ($entity->get('category') as $item) {
        if ($category = $item->entity) {
          $categories[] = $category->label();
        }
      }
    }

    $row = [
      'label' => $entity->getAdministrativeLabel(),
      'aid' => $entity->id(),
      'machine_name' => $entity->getMachineName(),
      'value_type' => $entity->getValueType(),
      'category' => implode(', ', $categories),
      'weight' => $entity->getWeight(),
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'eav_attribute_list';
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $form = \Drupal::formBuilder()->getForm($this);

    $form['pager'] = [
      '#type' => 'pager',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $filter_text = \Drupal::request()->query->get('filter_text');

    $form['filter'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['container-inline'],
      ],
      '#weight' => -100,
    ];

    $form['filter']['filter_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filter'),
      '#default_value' => $filter_text,
      '#size' => 30,
    ];

    $form['filter']['filter_submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      '#submit' => ['::filterSubmit'],
    ];

    $form['eav_attributes'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => t('Empty.'),
      '#tabledrag' => [[
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => 'weight',
      ]],
      '#weight' => -99,
    ];

    $eav_attributes = $this->load(); /** @var EavAttributeInterface[] $eav_attributes */
    foreach ($eav_attributes as $eav_attribute) {
      $row = $this->buildRow($eav_attribute);

      $row['label'] = ['#markup' => $row['label']];
      $row['aid'] = ['#markup' => $row['aid']];
      $row['machine_name'] = ['#markup' => $row['machine_name']];
      $row['value_type'] = ['#markup' => $row['value_type']];
      $row['category'] = ['#markup' => $row['category']];

      $row['#attributes']['class'][] = 'draggable';
      $row['#weight'] = $row['weight'];

      $row['weight'] = [
        '#type' => 'weight',
        '#delta' => 1000,
        '#default_value' => $row['#weight'],
        '#attributes' => [
          'class' => ['weight'],
        ],
      ];

      $form['eav_attributes'][$eav_attribute->id()] = $row;
    }

    if (!$filter_text) {
      $form['actions']['#type'] = 'actions';

      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => t('Save'),
        '#button_type' => 'primary',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    foreach ($form_state->getValue('eav_attributes') as $eav_attribute_id => $row_values) {
      $eav_attribute = EavAttribute::load($eav_attribute_id);
      if ($eav_attribute->getWeight() != $row_values['weight']) {
        $eav_attribute->set('weight', $row_values['weight']);
        $eav_attribute->save();
      }
    }

    $this->messenger()->addMessage($this->t('Configuration saved.'));
  }

  /**
   * Filter submit callback.
   */
  public function filterSubmit(array &$form, FormStateInterface $form_state): void {
    $form_state->setRedirect('entity.eav_attribute.collection', [], [
      'query' => [
        'filter_text' => $form_state->getValue('filter_text'),
      ],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds(): array {
    $query = $this->storage->getQuery()->sort('weight');
    $query->pager($this->limit);

    if ($filter_text = \Drupal::request()->query->get('filter_text')) {
      $filter_text_escaped = \Drupal::database()->escapeLike($filter_text);
      $or = $query->orConditionGroup();
      $or->condition('label', '%' . $filter_text_escaped . '%', 'LIKE');
      $or->condition('machine_name', '%' . $filter_text_escaped . '%', 'LIKE');
      $or->condition('aid', $filter_text);
      $query->condition($or);
    }

    return $query->accessCheck(FALSE)->execute();
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity): array {
    $operations = parent::getDefaultOperations($entity);
    $attribute_id = $entity->id();

    $operations['edit_storage'] = [
      'title' => $this->t('Storage settings'),
      'url' => Url::fromRoute('entity.eav_attribute.value_storage_form', ['eav_attribute' => $attribute_id]),
      'weight' => 20,
    ];

    $operations['edit_field'] = [
      'title' => $this->t('Field settings'),
      'url' => Url::fromRoute('entity.eav_attribute.value_field_form', ['eav_attribute' => $attribute_id]),
      'weight' => 30,
    ];

    $operations['edit_widget'] = [
      'title' => $this->t('Widget settings'),
      'url' => Url::fromRoute('entity.eav_attribute.value_widget_form', ['eav_attribute' => $attribute_id]),
      'weight' => 40,
    ];

    $operations['edit_formatter'] = [
      'title' => $this->t('Formatter settings'),
      'url' => Url::fromRoute('entity.eav_attribute.value_formatter_form', ['eav_attribute' => $attribute_id]),
      'weight' => 50,
    ];

    return $operations;
  }

}
